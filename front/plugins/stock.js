const locations = {
  warehouse: {
    name: "Склад",
  },
  area_pila: {
    name: "Участок пилы"
  }
};

export default ({store}, inject) => {
  const getStockItemStrings = ({remaining_length, melting_number}) => {
    let strings = [
      ((melting_number) ? `№ Плавки: ${melting_number}` : ``),
      ((remaining_length) ? `Осталось длины: ${remaining_length}мм` : ``),
    ];
    return strings.filter(s => s !== ``);
  }
  inject('getStockItemStrings', getStockItemStrings);

  const getStockItemLocations = () => {
    return locations;
  }
  inject('getStockItemLocations', getStockItemLocations);
}
