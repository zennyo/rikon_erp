export const roles = {
  // management

  management_tech: {
    name: "Технический отдел",
  },
  management_storage: {
    name: "Склад",
  },
  management_supply: {
    name: "Снабжение",
    permissions: ["stock", "stock_warehouse", "requests", "materials", "redirect_request_to_control", "redirect_request_to_tech"],
  },
  management_control: {
    name: "ОТК"
  },
  management_procurement: {
    name: "Заготовительный участок"
  },
  management_procurement_master: {
    name: "Заготовительный участок (мастер)"
  },
  management_assembly: {
    name: "Сборочный участок"
  },
  management_assembly_master: {
    name: "Сборочный участок (мастер)"
  },
  management_cutting: {
    name: "Отдел раскроя"
  },
  management_commercial: {
    name: "Коммерческий отдел"
  },
  management_sales: {
    name: "Отдел продаж"
  },
  management_hr: {
    name: "Отдел кадров"
  },
  management_accounting: {
    name: "Отдел бухгалтерии"
  },
  management_ceo: {
    name: "Исполнительный директор",
    permissions: '*',
  },
  management_cto: {
    name: "Технический директор",
    permissions: '*',
  },

  // areas

  area_service: {
    name: "Участок сервиса и монтажа"
  },
  area_equipment: {
    name: "Участок комплектации"
  },
  area_fas: {
    name: "Участок фасок"
  },
  area_pila: {
    name: "Участок пилы",
    permissions: ["area_pila", "tasks_list", "requests", "stock", "stock_area_pila"],
  },
  area_svr: {
    name: "Участок сверловки"
  },
  area_tok: {
    name: "Участок токарных работ"
  },
  area_frz: {
    name: "Участок фрезерных работ"
  },
  area_ras: {
    name: "Участок расточных работ"
  },
  area_gib: {
    name: "Участок гибочных работ"
  },
  area_cooperation: {
    name: "Участок кооперации"
  },
  area_procurement: {
    name: "Участок заготовки"
  },
  area_assembly: {
    name: "Участок сборки и сварки"
  },
  area_paint: {
    name: "Участок покраски"
  },

}

export default ({store}, inject) => {
  const getRoleName = (role_id) => {
    return roles[role_id].name;
  }
  inject('getRoleName', getRoleName)

  const isAllowed = (permission) => {
    if (!store.state.role) {
      return false;
    }
    let role_data = roles[store.state.role];
    let {permissions} = role_data;
    return permissions && (permissions === "*" || permissions.includes(permission));
  }
  inject('isAllowed', isAllowed);
}
