export const operations = {
  FAS: {
    name: "Фаска",
  },
  PILA: {
    name: "Пила",
  },
  SVR: {
    name: "Сверловка",
  },
  TOK: {
    name: "Токарные работы",
  },
  FRZ: {
    name: "Фрезерные работы",
  },
  RAS: {
    name: "Расточные работы",
  },
  GIB: {
    name: "Гибочные работы",
  },
}

export default ({store}, inject) => {
  const getOperations = () => {
    return operations
  }
  inject('getOperations', getOperations)
}
