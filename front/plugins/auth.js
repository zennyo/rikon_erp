export default ({ $axios, store, redirect }) => {
  if (process.server) {
    return
  }

  $axios.interceptors.request.use(request => {
    const token = store.state.token
    if (token) {
      request.headers.common['Token'] = token
    }
    return request
  })

  $axios.interceptors.response.use(response => {
    return response;
  }, error => {
    if (!error.response) {
      console.log(error);
      //redirect("/auth");
      return Promise.reject(error);
    }
    if (error.response.status === 403) {
      redirect("/auth");
    } else {
      return Promise.reject(error);
    }
  });
}
