export default ({store}, inject) => {
  const getMaterialSizeString = (s) => {
    if (!s) {
      return "";
    }
    let size = [];
    if (s.width && s.length) {
      size.push(`${s.width}x${s.length}`)
    } else if (s.length) {
      size.push(`L=${s.length}`)
    }
    if (s.d_outer) {
      size.push(`D=${s.d_outer}`)
    }
    if (s.d_inner) {
      size.push(`d=${s.d_inner}`)
    }
    return size.join(' ');
  }

  inject('getMaterialSizeString', getMaterialSizeString);

  const sizeToObject = (size) => {
    let res = {};
    let wl_matches = size.match(/([0-9]*)[xх*]([0-9]*)/);
    if (wl_matches) {
      res.width = Number(wl_matches[1]);
      res.length = Number(wl_matches[2]);
    }
    let l_matches = size.match(/L=([0-9]*)/);
    if (l_matches) {
      res.length = Number(l_matches[1]);
    }
    let d_outer_matches = size.match(/D=([0-9]*)/);
    if (d_outer_matches) {
      res.d_outer = Number(d_outer_matches[1]);
    }
    let d_inner_matches = size.match(/d=([0-9]*)/);
    if (d_inner_matches) {
      res.d_inner = Number(d_inner_matches[1]);
    }
    if (Object.keys(res).length === 0 && !!size) {
      res.length = Number(size);
    }
    return res;
  }

  inject('sizeToObject', sizeToObject);



  const getMaterialFeaturesStrings = (features) => {
    if (!features)
      return [undefined];

    let size = getMaterialSizeString(features.size);

    let strings = [
      ((features.mass) ? `Масса: ${features.mass}кг` : ``),
      ((size.length > 0) ? `Размер: ${size}` : ``),
      ((features.grade) ? `Марка: ${features.grade}` : ``),
      ((features.s) ? `S: ${features.s}` : ``),
      ((features.en) ? `EN: ${features.en}` : ``),
    ];
    return strings.filter(s => s !== ``);
  }
  inject('getMaterialFeaturesStrings', getMaterialFeaturesStrings);
}
