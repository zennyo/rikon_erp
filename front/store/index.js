export const state = () => ({
  username: null,
  token: null,
  role: null,
})

export const mutations = {
  setUser(state, {username, token, role}) {
    state.username = username;
    state.token = token;
    state.role = role;
  }
}
