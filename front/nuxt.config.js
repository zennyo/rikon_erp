export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'rikon_erp_front',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/sandstone/bootstrap.min.css',
    '@/assets/css/style.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "~/plugins/auth.js",
    "~/plugins/roles.js",
    "~/plugins/persistedState.client.js",
    "~/plugins/materials.js",
    "~/plugins/operations.js",
    "~/plugins/stock.js",
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',

    '@nuxtjs/apollo',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.NUXT_APP_API_ENDPOINT,
  },

  env: {
    NUXT_APP_API_ENDPOINT: process.env.NUXT_APP_API_ENDPOINT,
  },

  bootstrapVue: {
    icons: true
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    loaders: {
      vue: {
        transpileOptions: {
          transforms: {
            dangerousTaggedTemplateString: true
          }
        }
      }
    }
  },

  apollo: {
    clientConfigs: {
      default: {
        httpEndpoint: process.env.NUXT_APP_APOLLO_ENDPOINT,
      }
    }
  },

  ssr: false,
}
