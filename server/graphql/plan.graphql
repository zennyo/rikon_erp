type PlanSize {
    length: Int
    width: Int
    d_outer: Float
    d_inner: Float
}

type Plan implements Item {
    id: ItemId!
    order: Order!
    material: Material!
    detail: Detail!
    element: Element!
    comment: String!
    amount: Int!
    unit_mass: Float!
    unit_size: PlanSize!
    is_deleted: Boolean!

    operations(page: Page = {
        page: 1,
        per_page: 10
    }, filter: PlanOperationsFilter = {
        operation_code: null
        priority: null
        show_deleted: false
    }): PlanOperationsResult!
}

input PlanInput {
    material_id: ItemId!
    detail_id: ItemId!
    element_id: ItemId!
    amount: Int!
    unit_mass: Float!
    unit_size: String!
    comment: String!
}

input ImportPlanInput {
    material: MaterialInput!
    detail: DetailInput!
    element: ElementInput!
    amount: Int!
    unit_mass: Float!
    unit_size: String!
    comment: String!
}

input ImportPlansInput {
    items: [ImportPlanInput!]!
}

input PlanFilter {
    name: String
    number: String
    show_deleted: Boolean
}

type PlansResult implements Result {
    count: Int!
    items: [Plan!]
}

type PlanMutation {
    id: ItemId!
    edit(data: PlanInput!): ItemId!
    delete: Boolean

    createOperation(data: PlanOperationInput!): ItemId!
    operation(code: String!): PlanOperationMutation
}
