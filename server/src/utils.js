
export function sizeToObject (size) {
    let res = {};
    let wl_matches = size.match(/([0-9]*)[xх*]([0-9]*)/);
    if (wl_matches) {
        res.width = Number(wl_matches[1]);
        res.length = Number(wl_matches[2]);
    }
    let l_matches = size.match(/L=([0-9]*)/);
    if (l_matches) {
        res.length = Number(l_matches[1]);
    }
    let d_outer_matches = size.match(/D=([0-9]*)/);
    if (d_outer_matches) {
        res.d_outer = Number(d_outer_matches[1]);
    }
    let d_inner_matches = size.match(/d=([0-9]*)/);
    if (d_inner_matches) {
        res.d_inner = Number(d_inner_matches[1]);
    }
    if (Object.keys(res).length === 0 && !!size) {
        res.length = Number(size);
    }
    return res;
}

