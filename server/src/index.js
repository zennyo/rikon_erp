import 'dotenv/config'

import fs from 'fs'
import path from 'path'

import express from 'express';
import {initPrisma} from './prisma';
import resolvers from './resolvers/index'
import {ApolloServer} from "apollo-server-express"

initPrisma();

function readAllFiles(p) {
    let data = [];
    let files = fs.readdirSync(p);
    files.forEach(file => {
        let fp = path.join(p, file)
        if (fs.lstatSync(fp).isFile()) {
            data.push(fs.readFileSync(fp, 'utf-8'));
        }
    })
    return data.join('\n');
}

const typeDefs = readAllFiles('graphql');

const app = express();
app.use(express.json({ limit: '1mb' }));

const server = new ApolloServer({ resolvers, typeDefs,  })
server.start().then(() => {
    server.applyMiddleware({ app });

    app.listen({ port: process.env.APP_PORT }, () =>
        console.log(`🚀 Server running at http://localhost:${process.env.APP_PORT}${server.graphqlPath}`)
    );
});
