import {prisma} from "../prisma";

export default {
    async operations(parent, {page: {per_page, page}, filter: {operation_code, priority, show_deleted}}, context, info) {
        let operation;
        if (operation_code) {
            operation = await prisma.operation.findUnique({
                where: {
                    code: operation_code
                }
            })
        }
        let where = {
            operation_id: operation?.id ?? undefined,
            priority: priority ?? undefined,
            ...(show_deleted ? {} : {
                is_deleted: false,
            })
        };
        let items = await prisma.planOperation.findMany({
            skip: (page - 1) * per_page,
            take: per_page,
            where,
            include: {
                operation: true,
            }
        });
        let count = await prisma.planOperation.count({where});
        return {
            items,
            count,
        }
    },
}