import {prisma} from "../prisma";

export default {
    async createUser(parent, {data}, context, info) {
        let entry = await prisma.user.create({
            data
        })
        return entry.id;
    },

    async user(parent, {id}, context, info) {
        return {id};
    },

    async createOperation(parent, {data}, context, info) {
        let entry = await prisma.operation.create({
            data
        })
        return entry.id;
    },

    async operation(parent, {id}, context, info) {
        return {id};
    },

    async createOrder(parent, {data}, context, info) {
        let entry = await prisma.order.create({
            data
        })
        return entry.id;
    },

    async order(parent, {id}, context, info) {
        return {id};
    },

    async material(parent, {id}, context, info) {
        return {id};
    },

    async detail(parent, {id}, context, info) {
        return {id};
    },

    async element(parent, {id}, context, info) {
        return {id};
    },

    async message(parent, {id}, context, info) {
        return {id};
    },

    async plan(parent, {id}, context, info) {
        return {id};
    },
}