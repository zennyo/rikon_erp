import {prisma} from "../prisma";

export default {
    async users(parent, {page: {per_page, page}, filter: {name, username, show_deleted}}, context, info) {
        let where = {
            ...(!name ? {} : {
                name: {
                    contains: name,
                    mode: 'insensitive',
                }
            }),
            ...(!username ? {} : {
                username: {
                    contains: username,
                    mode: 'insensitive',
                }
            }),
            ...(show_deleted ? {} : {
                is_deleted: false,
            })
        }
        let items = await prisma.user.findMany({
            skip: (page - 1) * per_page,
            take: per_page,
            where,
        });
        let count = await prisma.user.count({where});
        return {
            items,
            count,
        }
    },
    async user(parent, {id}, context, info) {
        return await prisma.user.findUnique({
            where: {id}
        });
    },

    async operations(parent, {page: {per_page, page}, filter: {code, name}}, context, info) {
        let where = {
            ...(!code ? {} : {
                code: {
                    contains: code,
                    mode: 'insensitive',
                }
            }),
            ...(!name ? {} : {
                name: {
                    contains: name,
                    mode: 'insensitive',
                }
            }),
        }
        let items = await prisma.operation.findMany({
            skip: (page - 1) * per_page,
            take: per_page,
            where,
        });
        let count = await prisma.operation.count({where});
        return {
            items,
            count,
        }
    },
    async operation(parent, {id}, context, info) {
        return await prisma.operation.findUnique({
            where: {id}
        });
    },

    async orders(parent, {page: {per_page, page}, filter: {name, show_deleted}}, context, info) {
        let where = {
            ...(!name ? {} : {
                name: {
                    contains: name,
                    mode: 'insensitive',
                }
            }),
            ...(show_deleted ? {} : {
                is_deleted: false,
            })
        }
        let items = await prisma.order.findMany({
            skip: (page - 1) * per_page,
            take: per_page,
            where,
        });
        let count = await prisma.order.count({where});
        return {
            items,
            count,
        }
    },
    async order(parent, {id}, context, info) {
        return await prisma.order.findUnique({
            where: {id}
        });
    },
    async material(parent, {id}, context, info) {
        return await prisma.material.findUnique({
            where: {id}
        });
    },
    async detail(parent, {id}, context, info) {
        return await prisma.detail.findUnique({
            where: {id}
        });
    },
    async element(parent, {id}, context, info) {
        return await prisma.element.findUnique({
            where: {id}
        });
    },
    async plan(parent, {id}, context, info) {
        return await prisma.plan.findUnique({
            where: {id},
            include: {
                order: true,
                material: true,
                detail: true,
                element: true,
            },
        });
    }
}
