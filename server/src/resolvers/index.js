import Query from './Query';
import Mutation from "./Mutation";
import UserMutation from "./mutations/UserMutation";
import {GraphQLScalarType, Kind} from "graphql";
import OperationMutation from "./mutations/OperationMutation";
import OrderMutation from "./mutations/OrderMutation";
import Order from "./Order";
import MaterialMutation from "./mutations/MaterialMutation";
import DetailMutation from "./mutations/DetailMutation";
import ElementMutation from "./mutations/ElementMutation";
import GraphQLDateTime from 'graphql-type-datetime';
import MessageMutation from "./mutations/MessageMutation";
import PlanMutation from "./mutations/PlanMutation";
import Plan from "./Plan";

const ItemId = new GraphQLScalarType({
    name: 'ItemId',
    description: 'Item ID used in database',
    serialize(value) {
        return value;
    },
    parseValue(value) {
        return (value === null || value === 'null') ? null : parseInt(value);
    },
    parseLiteral(ast) {
        if (ast.kind === Kind.INT) {
            return parseInt(ast.value)
        }
        return null;
    },
});

export default {
    ItemId,
    DateTime: GraphQLDateTime,
    Query,
    Mutation,
    Order,
    Plan,
    UserMutation,
    OperationMutation,
    OrderMutation,
    MaterialMutation,
    DetailMutation,
    ElementMutation,
    MessageMutation,
    PlanMutation,
}