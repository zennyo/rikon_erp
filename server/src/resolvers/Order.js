import {prisma} from "../prisma";

export default {
    async materials(parent, {page: {per_page, page}, filter: {name, features, show_deleted}}, context, info) {
        let {s, grade} = features ?? {};
        let where = {
            ...(!name ? {} : {
                name: {
                    contains: name,
                    mode: 'insensitive',
                }
            }),
            ...(show_deleted ? {} : {
                is_deleted: false,
            }),
            AND: [
                ...(!s ? [] : [{
                    features: {
                        path: ['s'],
                        equals: s
                    }
                }]),
                ...(!grade ? [{}] : [{
                    features: {
                        path: ['grade'],
                        string_contains: grade,
                    }
                }]),
            ],
            order_id: parent.id,
        }
        let items = await prisma.material.findMany({
            skip: (page - 1) * per_page,
            take: per_page,
            where,
        });
        let count = await prisma.material.count({where});
        return {
            items,
            count,
        }
    },

    async details(parent, {page: {per_page, page}, filter: {name, number, show_deleted}}, context, info) {
        let where = {
            ...(!name ? {} : {
                name: {
                    contains: name,
                    mode: 'insensitive',
                }
            }),
            ...(!number ? {} : {
                number: {
                    contains: number,
                    mode: 'insensitive',
                }
            }),
            ...(show_deleted ? {} : {
                is_deleted: false,
            }),
            order_id: parent.id,
        }
        let items = await prisma.detail.findMany({
            skip: (page - 1) * per_page,
            take: per_page,
            where,
        });
        let count = await prisma.detail.count({where});
        return {
            items,
            count,
        }
    },

    async elements(parent, {page: {per_page, page}, filter: {name, number, show_deleted}}, context, info) {
        let where = {
            ...(!name ? {} : {
                name: {
                    contains: name,
                    mode: 'insensitive',
                }
            }),
            ...(!number ? {} : {
                number: {
                    contains: number,
                    mode: 'insensitive',
                }
            }),
            ...(show_deleted ? {} : {
                is_deleted: false,
            }),
            order_id: parent.id,
        }
        let items = await prisma.element.findMany({
            skip: (page - 1) * per_page,
            take: per_page,
            where,
        });
        let count = await prisma.element.count({where});
        return {
            items,
            count,
        }
    },

    async messages(parent, {page: {per_page, page}, filter: {message, sender_id, show_deleted}}, context, info) {
        let where = {
            ...(!message ? {} : {
                message: {
                    contains: message,
                    mode: 'insensitive',
                }
            }),
            ...(!sender_id ? {} : {
                sender_id: sender_id,
            }),
            ...(show_deleted ? {} : {
                is_deleted: false,
            }),
            chat_type: 'ORDER',
            chat_id1: parent.id,
            chat_id2: null,
        }
        let items = await prisma.message.findMany({
            skip: (page - 1) * per_page,
            take: per_page,
            where,
            include: {
                sender: true,
            },
            orderBy: {
                id: 'desc',
            }
        });
        let count = await prisma.message.count({where});
        return {
            items,
            count,
        }
    },

    async plan(parent, {page: {per_page, page}, filter: {name, number, show_deleted}}, context, info) {
        let where_2 = [];
        if (name) {
            let materials = await prisma.material.findMany({
                where: {
                    order_id: parent.id,
                    name: {
                        contains: name,
                        mode: 'insensitive',
                    }
                }
            })
            if (materials.length > 0) {
                where_2.push({
                    material_id: {
                        in: materials.map(m => m.id),
                    }
                });
            }
        }
        if (name || number) {
            let details = await prisma.detail.findMany({
                where: {
                    order_id: parent.id,
                    ...(!name ? {} : {
                        name: {
                            contains: name,
                            mode: 'insensitive',
                        }
                    }),
                    ...(!number ? {} : {
                        number: {
                            contains: number,
                            mode: 'insensitive',
                        }
                    }),
                }
            })
            if (details.length > 0) {
                where_2.push({
                    detail_id: {
                        in: details.map(m => m.id),
                    }
                });
            }
        }
        if (name || number) {
            let elements = await prisma.element.findMany({
                where: {
                    order_id: parent.id,
                    ...(!name ? {} : {
                        name: {
                            contains: name,
                            mode: 'insensitive',
                        }
                    }),
                    ...(!number ? {} : {
                        number: {
                            contains: number,
                            mode: 'insensitive',
                        }
                    }),
                }
            })
            if (elements.length > 0) {
                where_2.push({
                    element_id: {
                        in: elements.map(m => m.id),
                    }
                });
            }
        }
        if ((name || number) && where_2.length === 0) {
            return {
                count: 0,
                items: [],
            };
        }
        let where = {
            order_id: parent.id,
            ...(show_deleted ? {} : {
                is_deleted: false,
            }),
            OR: (where_2.length === 0) ? undefined : where_2,
        }
        let items = await prisma.plan.findMany({
            skip: (page - 1) * per_page,
            take: per_page,
            where,
            include: {
                material: true,
                detail: true,
                element: true,
            },
            orderBy: {
                id: 'asc',
            }
        });
        let count = await prisma.plan.count({where});
        return {
            items,
            count,
        }
    },
}