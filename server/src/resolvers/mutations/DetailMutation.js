import {prisma} from "../../prisma";

export default {
    async edit(parent, {data}, context, info) {
        let prev = await prisma.detail.findUnique({
            where: {id: parent.id}
        })
        await prisma.detail.update({
            where: {id: parent.id},
            data: {
                is_deleted: true,
            }
        })
        let entry = await prisma.detail.create({
            data: {
                ...prev,
                id: undefined,
                ...data
            }
        })
        return entry.id;
    },

    async delete(parent, {}, context, info) {
        await prisma.detail.update({
            where: {id: parent.id},
            data: {
                is_deleted: true,
            }
        })
    },
}