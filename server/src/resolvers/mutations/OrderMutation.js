import {prisma} from "../../prisma";
import {sizeToObject} from "../../utils";

export default {
    async edit(parent, {data}, context, info) {
        let entry = await prisma.order.update({
            where: {id: parent.id},
            data
        })
        return entry.id;
    },

    async delete(parent, {}, context, info) {
        await prisma.order.update({
            where: {id: parent.id},
            data: {
                is_deleted: true,
            }
        })
    },

    async createMaterial(parent, {data}, context, info) {
        let entry = await prisma.material.create({
            data: {
                ...data,
                order_id: parent.id,
            }
        })
        return entry.id;
    },

    async createDetail(parent, {data}, context, info) {
        let entry = await prisma.detail.create({
            data: {
                ...data,
                order_id: parent.id,
            }
        })
        return entry.id;
    },

    async createElement(parent, {data}, context, info) {
        let entry = await prisma.element.create({
            data: {
                ...data,
                order_id: parent.id,
            }
        })
        return entry.id;
    },

    async createMessage(parent, {data}, context, info) {
        let entry = await prisma.message.create({
            data: {
                ...data,
                chat_type: 'ORDER',
                chat_id1: parent.id,
                chat_id2: null,
                sender: {
                    connect: {
                        id: 1,
                    }
                },
            }
        })
        return entry.id;
    },

    async createPlan(parent, {data}, context, info) {

        let unit_size = sizeToObject(data.unit_size);

        let entry = await prisma.plan.create({
            data: {
                ...{
                    ...data,
                    order_id: undefined,
                    material_id: undefined,
                    detail_id: undefined,
                    element_id: undefined,
                    unit_size: undefined,
                },
                unit_size,
                order: {
                    connect: {
                        id: parent.id,
                    }
                },
                material: {
                    connect: {
                        id: data.material_id,
                    }
                },
                detail: {
                    connect: {
                        id: data.detail_id,
                    }
                },
                element: {
                    connect: {
                        id: data.element_id,
                    }
                },
            }
        })
        return entry.id;
    },
    async importPlan(parent, {items}, context, info) {
        //await prisma.$transaction(async (prisma) => {
        for (let item of items.items) {
            let {material, detail, element, amount, unit_mass, unit_size, comment} = item;

            let {s, grade} = material.features ?? {};
            let material_where = {
                name: material.name,
                AND: [
                    ...(!s ? [] : [{
                        features: {
                            path: ['s'],
                            equals: s
                        }
                    }]),
                    ...(!grade ? [{}] : [{
                        features: {
                            path: ['grade'],
                            string_contains: grade,
                        }
                    }]),
                ],
                order_id: parent.id,
                is_deleted: false,
            }
            let m = await prisma.material.findFirst({where: material_where});
            if (!m) {
                m = await prisma.material.create({
                    data: {
                        ...material,
                        order: {
                            connect: {
                                id: parent.id,
                            }
                        },
                    }
                })
            }
            let material_id = m.id;

            let detail_where = {
                number: detail.number,
                order_id: parent.id,
                is_deleted: false,
            }
            let d = await prisma.detail.findFirst({where: detail_where});
            if (!d) {
                d = await prisma.detail.create({
                    data: {
                        ...detail,
                        order: {
                            connect: {
                                id: parent.id,
                            }
                        },
                    }
                })
            }
            let detail_id = d.id;

            let element_where = {
                number: element.number,
                order_id: parent.id,
                is_deleted: false,
            }
            let e = await prisma.element.findFirst({where: element_where});
            if (!e) {
                e = await prisma.element.create({
                    data: {
                        ...element,
                        order: {
                            connect: {
                                id: parent.id,
                            }
                        },
                    }
                })
            }
            let element_id = e.id;

            unit_size = sizeToObject(unit_size);

            let entry = await prisma.plan.create({
                data: {
                    order: {
                        connect: {
                            id: parent.id,
                        }
                    },
                    material: {
                        connect: {
                            id: material_id,
                        }
                    },
                    detail: {
                        connect: {
                            id: detail_id,
                        }
                    },
                    element: {
                        connect: {
                            id: element_id,
                        }
                    },
                    amount,
                    unit_mass,
                    unit_size,
                    comment,
                }
            })
        }
        //})
    }
}