import {prisma} from "../../prisma";

export default {
    async edit(parent, {data}, context, info) {
        let prev = await prisma.material.findUnique({
            where: {id: parent.id}
        })
        await prisma.material.update({
            where: {id: parent.id},
            data: {
                is_deleted: true,
            }
        })
        let entry = await prisma.material.create({
            data: {
                ...prev,
                id: undefined,
                ...data
            }
        })
        return entry.id;
    },

    async delete(parent, {}, context, info) {
        await prisma.material.update({
            where: {id: parent.id},
            data: {
                is_deleted: true,
            }
        })
    },
}