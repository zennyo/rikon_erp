import {prisma} from "../../prisma";

export default {
    async edit(parent, {data}, context, info) {
        let prev = await prisma.element.findUnique({
            where: {id: parent.id}
        })
        await prisma.element.update({
            where: {id: parent.id},
            data: {
                is_deleted: true,
            }
        })
        let entry = await prisma.element.create({
            data: {
                ...prev,
                id: undefined,
                ...data
            }
        })
        return entry.id;
    },

    async delete(parent, {}, context, info) {
        await prisma.element.update({
            where: {id: parent.id},
            data: {
                is_deleted: true,
            }
        })
    },
}