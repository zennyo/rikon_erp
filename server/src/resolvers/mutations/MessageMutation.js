import {prisma} from "../../prisma";

export default {
    async delete(parent, {}, context, info) {
        await prisma.message.update({
            where: {id: parent.id},
            data: {
                is_deleted: true,
            }
        })
    },
}