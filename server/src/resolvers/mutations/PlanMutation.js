import {prisma} from "../../prisma";
import {sizeToObject} from "../../utils";

export default {
    async edit(parent, {data}, context, info) {
        let unit_size = sizeToObject(data.unit_size);

        let entry = await prisma.plan.update({
            where: {id: parent.id},
            data: {
                ...{
                    ...data,
                    order_id: undefined,
                    material_id: undefined,
                    detail_id: undefined,
                    element_id: undefined,
                    unit_size: undefined,
                },
                unit_size: unit_size,
                material: {
                    connect: {
                        id: data.material_id,
                    }
                },
                detail: {
                    connect: {
                        id: data.detail_id,
                    }
                },
                element: {
                    connect: {
                        id: data.element_id,
                    }
                },
            }
        })
        return entry.id;
    },

    async delete(parent, {}, context, info) {
        await prisma.plan.update({
            where: {id: parent.id},
            data: {
                is_deleted: true,
            }
        })
    },

    async createOperation(parent, {data: {operation_id, priority}}, context, info) {
        let entry = await prisma.planOperation.create({
            data: {
                plan_id: parent.id,
                operation_id,
                priority,
            }
        })
        return entry.id;
    }
}