import {prisma} from "../../prisma";

export default {
    async edit(parent, {data}, context, info) {
        let entry = await prisma.operation.update({
            where: {id: parent.id},
            data
        })
        return entry.id;
    },

    async delete(parent, {}, context, info) {
        await prisma.operation.update({
            where: {id: parent.id},
            data: {
                is_deleted: true,
            }
        })
        //await prisma.operation.delete({where: {id: parent.id}});
    },
}